/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import godau.fynn.librariesdirect.model.ContextConsumer
import godau.fynn.librariesdirect.model.Library

@Deprecated("")
class AboutLibrariesActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        if (!intent.hasExtra("libraries")) {
            Log.e(
                "AboutLibrariesActivity",
                "Do not launch AboutLibrariesActivity directly, please. " +
                        "You need to construct an Intent for this activity using AboutLibrariesActivity.IntentBuilder."
            )
            Toast.makeText(this, "Activity launched incorrectly, see log", Toast.LENGTH_SHORT)
                .show()
            finish()
            return
        }

        // Casting to Library[] has crashed the app in the emulator on low API versions
        val libraries = intent.getSerializableExtra(EXTRA_LIBRARIES) as Array<Any>?
        val consumer = intent.getSerializableExtra(EXTRA_CONSUMER) as ContextConsumer?
        val header = intent.getStringExtra(EXTRA_HEADER)

        // Run consumer
        consumer?.accept(this)

        // Set layout after running consumer
        setContentView(R.layout.activity_about_libraries)
        val headerTextView = findViewById<TextView>(R.id.header_text)

        // Show header
        if (header == null) headerTextView.visibility =
            View.GONE else if (header != "#default") headerTextView.text = header

        // Show libraries
        val list = findViewById<ListView>(R.id.list)
        list.adapter = LibraryAdapter(this, libraries!!)
    }

    @Deprecated("")
    class IntentBuilder(private val context: Context) {
        private var libraries = arrayOf<Library>()
        private var consumer: ContextConsumer? = null
        private var headerText = "#default"

        /**
         * Set the array of libraries that should be displayed in AboutLibrariesActivity
         */
        fun setLibraries(libraries: Array<Library>): IntentBuilder {
            this.libraries = libraries
            return this
        }

        /**
         * In case you want to do something else in the AboutLibrariesActivity, e.g. set style dynamically,
         * pass a consumer that will be called from the activity with its context.
         *
         * @param consumer Consumer that will receive AboutLibraryActivity's context
         */
        fun setConsumer(consumer: ContextConsumer?): IntentBuilder {
            this.consumer = consumer
            return this
        }

        /**
         * Set the text that is displayed at the top of the list. If this method not called or called with
         * parameter "#default" the default text will be used. Pass null to hide the header text view.
         */
        fun setHeaderText(text: String): IntentBuilder {
            headerText = text
            return this
        }

        fun build(): Intent {
            return Intent(context, AboutLibrariesActivity::class.java)
                .putExtra(EXTRA_LIBRARIES, libraries)
                .putExtra(EXTRA_CONSUMER, consumer)
                .putExtra(EXTRA_HEADER, headerText)
        }
    }

    companion object {
        const val EXTRA_LIBRARIES = "libraries"
        const val EXTRA_CONSUMER = "consumer"
        const val EXTRA_HEADER = "header"
    }
}