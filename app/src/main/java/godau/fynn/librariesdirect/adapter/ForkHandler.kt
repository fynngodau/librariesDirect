/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.adapter

import android.view.ViewGroup
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.createStringResHighlightPlaceholder
import godau.fynn.librariesdirect.model.Fork

class ForkHandler : ClickableHandler<Fork>() {
    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.row_fork, parent, false))
    }

    protected override fun getText(item: Fork): CharSequence {
        return createStringResHighlightPlaceholder(
            R.string.a_fork_of, item.name, context
        )
            .append("\n")
            .append(
                createStringResHighlightPlaceholder(
                    R.string.by_author, item.author, context
                )
            )
            .append("\n")
            .append(item.license.name)
    }
}