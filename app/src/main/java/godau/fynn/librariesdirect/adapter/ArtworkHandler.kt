/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.adapter

import android.text.SpannableStringBuilder
import android.view.ViewGroup
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.model.Artwork
import godau.fynn.librariesdirect.replaceWithFormatted

class ArtworkHandler : ClickableHandler<Artwork>() {
    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.row_artwork, parent, false))
    }

    override fun getText(item: Artwork): CharSequence {
        return replaceWithFormatted(
            SpannableStringBuilder(
                context.getString(R.string.asset_by_author, item.name, '▶')
            ),
            '▶',
            item.author,
            createHighlightSpan()
        )
            .append("\n")
            .append(item.license.name)
    }
}