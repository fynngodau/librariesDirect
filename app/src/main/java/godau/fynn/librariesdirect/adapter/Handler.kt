package godau.fynn.librariesdirect.adapter

import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import godau.fynn.librariesdirect.R
import godau.fynn.typedrecyclerview.TypeHandler

abstract class Handler<H> : TypeHandler<Handler.ViewHolder, H>() {
    override fun bindViewHolder(holder: ViewHolder, item: H, position: Int) {
        holder.textView.text = getText(item)
    }

    protected fun createHighlightSpan(): ForegroundColorSpan {
        return godau.fynn.librariesdirect.createHighlightSpan(context)
    }

    protected abstract fun getText(item: H): CharSequence

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView: TextView
        var browseView: ImageView?

        init {
            textView = itemView.findViewById(R.id.text)
            browseView = itemView.findViewById(R.id.browse)
        }
    }
}