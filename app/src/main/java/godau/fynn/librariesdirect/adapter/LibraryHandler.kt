/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.adapter

import android.content.Context
import android.text.SpannableStringBuilder
import android.view.ViewGroup
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.createHighlightSpan
import godau.fynn.librariesdirect.createStringResHighlightPlaceholder
import godau.fynn.librariesdirect.model.Library

class LibraryHandler : ClickableHandler<Library>() {
    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.row_library, parent, false))
    }

    override fun getText(item: Library): CharSequence {
        return getText(item, context)
    }
}

/**
 * Static method for use in [godau.fynn.librariesdirect.LibraryAdapter]
 */
internal fun getText(item: Library, context: Context): CharSequence {
    return SpannableStringBuilder()
        .append(item.name)
        .apply {
            setSpan(createHighlightSpan(context), 0, item.name.length, 0)
        }
        .append("\n")
        .append(
            createStringResHighlightPlaceholder(
                R.string.by_author, item.author, context
            )
        )
        .append("\n")
        .append(item.license.name)
}