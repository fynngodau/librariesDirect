/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.adapter

import android.view.ViewGroup
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.createStringResHighlightPlaceholder
import godau.fynn.librariesdirect.model.Imprint
import godau.fynn.librariesdirect.model.OwnLicense

class ImprintHandler : Handler<Imprint>() {
    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.row_imprint, parent, false))
    }

    override fun getText(item: Imprint): CharSequence {
        return item.imprint
    }
}