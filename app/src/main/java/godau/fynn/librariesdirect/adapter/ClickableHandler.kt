package godau.fynn.librariesdirect.adapter

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.model.ClickableItem

abstract class ClickableHandler<H : ClickableItem> : Handler<H>() {
    override fun bindViewHolder(holder: ViewHolder, item: H, position: Int) {
        super.bindViewHolder(holder, item, position)

        item.url.let { url ->
            if (url != null) {
                holder.browseView?.visibility = View.VISIBLE
                holder.browseView?.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    context.startActivity(intent)
                }
            } else {
                holder.browseView?.visibility = View.GONE
            }
        }

        item.license.licenseText.let {
            if (it != null) {
                holder.textView.setOnClickListener {
                    AlertDialog.Builder(context)
                        .setTitle(R.string.license)
                        .setMessage(item.getDisplayMessage(context))
                        .show()
                }
            } else {
                holder.textView.setOnClickListener(null)
            }
        }
    }
}