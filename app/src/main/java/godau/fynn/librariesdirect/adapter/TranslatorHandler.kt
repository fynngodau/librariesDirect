/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.adapter

import android.text.SpannableStringBuilder
import android.view.ViewGroup
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.appendStringResHighlightPlaceholder
import godau.fynn.librariesdirect.model.Translator

class TranslatorHandler : Handler<Translator>() {
    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.row_translator, parent, false))
    }

    override fun getText(item: Translator): CharSequence {
        val builder = SpannableStringBuilder()
            .append(item.name)
            .apply {
                setSpan( createHighlightSpan(), 0, item.name.length, 0)
            }
        if (item.handle != null) {
            builder
                .append(" ")
                .append(item.handle)
        }
        return appendStringResHighlightPlaceholder(
            builder.append('\n'), R.string.translated_to, item.localeName, context
        )
    }
}