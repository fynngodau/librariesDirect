/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.adapter

import godau.fynn.librariesdirect.model.Artwork
import godau.fynn.librariesdirect.model.Contribution
import godau.fynn.librariesdirect.model.Fork
import godau.fynn.librariesdirect.model.Imprint
import godau.fynn.librariesdirect.model.Library
import godau.fynn.librariesdirect.model.OwnLicense
import godau.fynn.librariesdirect.model.Translator
import godau.fynn.typedrecyclerview.TypeHandler
import godau.fynn.typedrecyclerview.TypedRecyclerViewAdapter

typealias Separator = Unit

class ContentAdapter(content: Array<Any>) : TypedRecyclerViewAdapter<Any>(
    content.asSequence().withSeparators().toMutableList()
) {
    override fun getItemHandlerClass(item: Any, position: Int): Class<out TypeHandler<*, out Any>>? =
        when (item) {
            is Contribution -> ContributionHandler::class.java
            is Fork -> ForkHandler::class.java
            is Artwork -> ArtworkHandler::class.java
            is Library -> LibraryHandler::class.java
            is Translator -> TranslatorHandler::class.java
            is Separator -> SeparatorHandler::class.java
            is OwnLicense -> OwnLicenseHandler::class.java
            is Imprint -> ImprintHandler::class.java
            else -> null
        }

}

/**
 * Returns Array as List with [Separator] objects in between objects of different class
 */
private fun <A> Sequence<A>.withSeparators(): Sequence<Any> =
    fold(emptySequence()) { acc, add ->
        add as Any
        if (acc.lastOrNull()?.javaClass?.equals(add.javaClass) == false) {
            acc + Separator + add
        } else acc + add
    }