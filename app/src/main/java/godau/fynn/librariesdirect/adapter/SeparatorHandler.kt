/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import godau.fynn.librariesdirect.R
import godau.fynn.typedrecyclerview.TypeHandler

class SeparatorHandler : TypeHandler<RecyclerView.ViewHolder, Separator>() {
    override fun createViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return object : RecyclerView.ViewHolder(
            inflater.inflate(R.layout.separator, parent, false)
        ) {}
    }

    override fun bindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: Separator,
        position: Int
    ) = Unit
}