/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.FragmentActivity
import godau.fynn.librariesdirect.model.ContextConsumer

class AboutDirectActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        require(intent.hasExtra(EXTRA_BUNDLE)) {
            "Do not launch AboutDirectActivity directly, please. " +
                    "You need to construct an Intent for this activity using AboutDirectActivity.IntentBuilder."
        }
        val bundle = intent.getBundleExtra(EXTRA_BUNDLE)

        // Casting to Library[] has crashed the app in the emulator on low API versions
        val consumer = bundle!!.getSerializable(EXTRA_CONSUMER) as ContextConsumer?

        // Run consumer
        consumer?.accept(this)


        // Set layout after running consumer
        setContentView(R.layout.activity_about_direct)

        // Don't run twice
        bundle.remove(EXTRA_CONSUMER)

        if (savedInstanceState == null) supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment, AboutDirectFragment::class.java, bundle)
            .commit()
    }

    class IntentBuilder(
        private val context: Context,
        @StringRes appName: Int,
        appVersion: String?
    ) {
        private val bundle = Bundle()

        init {
            bundle.putInt(EXTRA_APP_NAME, appName)
            bundle.putString(EXTRA_APP_VERSION, appVersion)
        }

        /**
         * Pass the app developer's name to display it at the top below your app's name.
         */
        fun setAppDeveloperName(appDeveloperName: String): IntentBuilder {
            bundle.putString(EXTRA_APP_DEVELOPER_NAME, appDeveloperName)
            return this
        }

        /**
         * Pass a URL to display a Mastodon icon to the right of the developer's name that links to that URL.
         */
        fun setAppDeveloperMastodon(appDeveloperMastodon: String): IntentBuilder {
            bundle.putString(EXTRA_APP_DEVELOPER_MASTODON, appDeveloperMastodon)
            return this
        }

        /*
         * Pass an icon to display it at the top to the left of the app's name.
         */
        fun setIcon(@DrawableRes icon: Int): IntentBuilder {
            bundle.putInt(EXTRA_ICON, icon)
            return this
        }

        /**
         * Set the array of libraries that should be displayed in AboutLibrariesActivity
         */
        fun setContent(content: Array<Any>): IntentBuilder {
            bundle.putSerializable(EXTRA_CONTENT, content)
            return this
        }

        /**
         * In case you want to do something else in the AboutLibrariesActivity, e.g. set style dynamically,
         * pass a consumer that will be called from the activity with its context, or from the fragment with your
         * own activity's context.
         *
         * @param consumer Consumer that will receive AboutLibraryActivity's context
         */
        fun setConsumer(consumer: ContextConsumer): IntentBuilder {
            bundle.putSerializable(EXTRA_CONSUMER, consumer)
            return this
        }

        fun build(): Intent {
            return Intent(context, AboutDirectActivity::class.java)
                .putExtra(EXTRA_BUNDLE, bundle)
        }

        fun buildFragment(): AboutDirectFragment {
            val fragment = AboutDirectFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    companion object {
        const val EXTRA_CONTENT = "content"
        const val EXTRA_CONSUMER = "consumer"
        const val EXTRA_APP_NAME = "appName"
        const val EXTRA_APP_VERSION = "appVersion"
        const val EXTRA_APP_DEVELOPER_NAME = "appDeveloperName"
        const val EXTRA_APP_DEVELOPER_MASTODON = "appDeveloperMastodon"
        const val EXTRA_ICON = "icon"
        const val EXTRA_BUNDLE = "bundle"
    }
}