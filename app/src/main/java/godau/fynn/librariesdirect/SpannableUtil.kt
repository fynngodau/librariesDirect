package godau.fynn.librariesdirect

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.TypedValue
import androidx.annotation.ColorInt
import androidx.annotation.StringRes

fun createHighlightSpan(context: Context): ForegroundColorSpan {
    // Get attr for text color
    val typedValue = TypedValue()
    val textSizeAttr = intArrayOf(android.R.attr.textColorPrimary)
    val array = context.obtainStyledAttributes(typedValue.data, textSizeAttr)
    @ColorInt val textColor = array.getColor(0, -1)
    array.recycle()
    return ForegroundColorSpan(textColor)
}

/**
 * Replaces the given char in the given builder with the given String
 * and spans the given Object over the newly inserted text.
 */
fun replaceWithFormatted(
    builder: SpannableStringBuilder, replace: Char,
    replaceWith: String, spanOnReplacement: Any
): SpannableStringBuilder {
    // Find char to replace
    for (i in builder.indices) {
        if (builder[i] == replace) {
            builder.delete(i, i + 1)
            builder.insert(i, replaceWith)
            builder.setSpan(spanOnReplacement, i, i + replaceWith.length, 0)
            return builder
        }
    }
    throw IllegalArgumentException("Symbol $replace not in builder")
}

/**
 * Appends the given String from StringRes to the builder while highlighting
 * the placeholder in the string.
 */
fun appendStringResHighlightPlaceholder(
    builder: SpannableStringBuilder, @StringRes stringRes: Int, placeInPlaceholder: String,
    context: Context
): SpannableStringBuilder {
    builder.append(context.getString(stringRes, '▶'))
    return replaceWithFormatted(builder, '▶', placeInPlaceholder, createHighlightSpan(context))
}

/**
 * Creates a SpannableStringBuilder from the given StringRes while highlighting
 * the placeholder in the string.
 */
fun createStringResHighlightPlaceholder(
    @StringRes stringRes: Int, placeInPlaceholder: String, context: Context
): SpannableStringBuilder {
    return appendStringResHighlightPlaceholder(
        SpannableStringBuilder(), stringRes, placeInPlaceholder, context
    )
}