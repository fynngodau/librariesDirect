package godau.fynn.librariesdirect.model

import android.content.Context
import androidx.annotation.StringRes
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.model.Library
import java.io.Serializable

/**
 * Like [Library.Library]. Credits arbitrary
 * contributions to the app.
 */
data class Contribution @JvmOverloads constructor(
    override val license: License,
    override val foreword: String? = null,
    override val author: String,
    override val plural: Boolean = false,
    override val url: String? = null
) : ClickableItem(), Serializable {

    override val name: String
        get() = TODO("Does not exist")

    override fun getDisplayMessage(context: Context): String {
        @StringRes val stringId =
            if (plural) R.string.license_explanation_contribution_pl else R.string.license_explanation_contribution
        val explanation = context.getString(stringId, author, license.name)

        return listOfNotNull(explanation, foreword, license.licenseText).joinToString("\n\n")
    }
}