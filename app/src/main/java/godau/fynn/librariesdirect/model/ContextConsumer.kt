/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.model

import android.content.Context
import java.io.Serializable

/**
 * Utility interface to ensure that consumers that are passed to
 * `setConsumer(ContextConsumer)` are serializable.
 */
interface ContextConsumer : Serializable {
    fun accept(t: Context)
}