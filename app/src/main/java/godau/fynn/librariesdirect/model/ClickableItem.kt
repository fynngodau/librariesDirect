package godau.fynn.librariesdirect.model

import android.content.Context
import androidx.annotation.StringRes
import godau.fynn.librariesdirect.R
import java.io.Serializable

/**
 * Item which should allow displaying a license object as well
 * as opening a URL
 */
sealed class ClickableItem : Serializable {

    abstract val name: String
    abstract val license: License
    abstract val plural: Boolean
    abstract val foreword: String?
    abstract val author: String
    abstract val url: String?

    open fun getDisplayMessage(context: Context): String {
        @StringRes val stringId =
            if (plural) R.string.license_explanation_pl else R.string.license_explanation
        val explanation = context.getString(stringId, author, name, license.name)

        return listOfNotNull(
            explanation, foreword, license.licenseText
        ).joinToString("\n\n")
    }
}
