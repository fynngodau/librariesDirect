package godau.fynn.librariesdirect.model

import godau.fynn.librariesdirect.model.Library
import java.io.Serializable

/**
 * Like [Library.Library]. Credits the app as derived from
 * another project
 */
data class Fork @JvmOverloads constructor(
    override val name: String,
    override val license: License,
    override val foreword: String? = null,
    override val author: String,
    override val plural: Boolean = false,
    override val url: String? = null
) : ClickableItem(), Serializable