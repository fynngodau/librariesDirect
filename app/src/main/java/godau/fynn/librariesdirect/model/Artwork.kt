package godau.fynn.librariesdirect.model

import godau.fynn.librariesdirect.model.Library
import java.io.Serializable

/**
 * Like [Library.Library]. Credits an artwork contained
 * in the app. Specify what exactly it is using `what` (e.g. `"icon"`).
 * @param what What kind of artwork this is about
 */
data class Artwork @JvmOverloads constructor(
    val what: String,
    override val license: License,
    override val foreword: String? = null,
    override val author: String,
    override val plural: Boolean = false,
    override val url: String? = null
) : ClickableItem(), Serializable {
    override val name: String = what
}