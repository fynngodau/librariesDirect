/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.model

import java.io.Serializable

/**
 * @param name Name of the library
 * @param license A license object. The `License` class has a small selection of licenses already hardcoded for you, including MIT and Apache 2.0, but you can construct your own license using `new License(String name, String licenseText)` as well.
 * @param foreword Some people put one line or two of copyright statement above their paste of e.g. the MIT license. Use the foreword string in that case, or pass null.
 * @param author Should fit into this sentence: "`author` (is | are) distributing this library."
 * @param plural Pass true if more than one author is provided.
 * @param url The website that the library can be found at
 */
data class Library @JvmOverloads constructor(
    override val name: String,
    override val license: License,
    override val foreword: String? = null,
    override val author: String,
    override val plural: Boolean = false,
    override val url: String? = null
) : ClickableItem(), Serializable