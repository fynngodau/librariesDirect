/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.model

import java.io.Serializable
import java.util.Locale

/**
 * An individual who has contributed a translation to the software.
 * @param name   Name of the individual
 * @param handle Optionally the user handle of the individual
 * @param locale The language the user has translated to
 */
data class Translator(
    val name: String,
    val handle: String?,
    private val locale: Locale
    ) : Serializable {
    val localeName: String
        get() = locale.displayName
}