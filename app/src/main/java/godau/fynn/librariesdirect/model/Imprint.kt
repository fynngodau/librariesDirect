package godau.fynn.librariesdirect.model

import android.content.Context
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.model.Library
import java.io.Serializable
import kotlin.math.exp

/**
 * Will be displayed as text-only card, with no link.
 *
 * @param imprint Imprint to display
 */
data class Imprint constructor(
    val imprint: String
) : Serializable