package godau.fynn.librariesdirect.model

import android.content.Context
import godau.fynn.librariesdirect.R
import godau.fynn.librariesdirect.model.Library
import java.io.Serializable
import kotlin.math.exp

/**
 * Will be displayed as a single-line `"Licensed " + license` card, optionally
 * with an "open repo" link. Tapping the license opens it.
 *
 * @param license  License this app is licensed under
 * @param foreword License foreword, see [Library.Library]
 * @param url      Repo URL
 */
data class OwnLicense @JvmOverloads constructor(
    override val license: License,
    override val foreword: String? = null,
    override val url: String? = null
) :
    ClickableItem(), Serializable {

    override val name: String
        get() = TODO("Does not exist")

    override val plural: Boolean
        get() = TODO("Does not exist")
    override val author: String
        get() = TODO("Does not exist")

    override fun getDisplayMessage(context: Context): String {
        val explanation = context.getString(R.string.license_explanation_own, license.name)
        return listOfNotNull(explanation, foreword, license.licenseText).joinToString("\n\n")
    }
}