package godau.fynn.librariesdirect

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import godau.fynn.librariesdirect.adapter.ContentAdapter
import godau.fynn.librariesdirect.model.ContextConsumer

class AboutDirectFragment : Fragment() {
    private var bundle: Bundle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireNotNull(arguments) {
            "Do not construct AboutDirectFragment directly, please. " +
                    "You need to construct an Intent for this fragment using AboutDirectActivity.IntentBuilder."
        }

        bundle = arguments

        val consumer =
            bundle!!.getSerializable(AboutDirectActivity.Companion.EXTRA_CONSUMER) as ContextConsumer?

        // Run consumer
        consumer?.accept(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_about_direct, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val content =
            bundle!!.getSerializable(AboutDirectActivity.EXTRA_CONTENT) as Array<Any>

        // Show app metadata
        @StringRes val appName = bundle!!.getInt(AboutDirectActivity.EXTRA_APP_NAME, -1)
        val appNameView = view.findViewById<TextView>(R.id.app_name)
        appNameView.setText(appName)

        val appVersion = bundle!!.getString(AboutDirectActivity.EXTRA_APP_VERSION)

        val appDeveloperName =
            bundle!!.getString(AboutDirectActivity.EXTRA_APP_DEVELOPER_NAME)
        val appVersionAuthorView = view.findViewById<TextView>(R.id.app_version_author)

        // Author and version information
        val authorVersionText: CharSequence? = if (appVersion != null && appDeveloperName != null) {
            replaceWithFormatted(
                SpannableStringBuilder(
                    getString(
                        R.string.asset_by_author,
                        getString(R.string.app_version, appVersion),
                        '▶'
                    )
                ),
                '▶',
                appDeveloperName,
                createHighlightSpan(requireContext())
            )
        } else if (appVersion != null) {
            getString(R.string.app_version, appVersion)
        } else if (appDeveloperName != null) {
            createStringResHighlightPlaceholder(
                R.string.by_author, appDeveloperName, requireContext()
            )
        } else null

        if (authorVersionText != null) {
            appVersionAuthorView.visibility = View.VISIBLE
            appVersionAuthorView.text = authorVersionText
        } else {
            appVersionAuthorView.visibility = View.GONE
        }

        val appDeveloperMastodon =
            bundle!!.getString(AboutDirectActivity.EXTRA_APP_DEVELOPER_MASTODON)
        if (appDeveloperMastodon != null) {
            view.findViewById<View>(R.id.mastodon).setOnClickListener {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(appDeveloperMastodon)
                    )
                )
            }
        } else {
            view.findViewById<View>(R.id.mastodon).visibility = View.GONE
        }

        @DrawableRes val icon = bundle!!.getInt(AboutDirectActivity.EXTRA_ICON, -1)
        val iconView = view.findViewById<ImageView>(R.id.app_icon)
        if (icon != -1) {
            iconView.setImageDrawable(requireContext().resources.getDrawable(icon))
        } else {
            iconView.visibility = View.GONE
        }

        // Show libraries
        val list = view.findViewById<RecyclerView>(R.id.list)
        list.layoutManager = LinearLayoutManager(requireContext())
        list.adapter = ContentAdapter(content)
    }
}