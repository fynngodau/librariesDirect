/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import godau.fynn.librariesdirect.adapter.LibraryHandler
import godau.fynn.librariesdirect.adapter.getText
import godau.fynn.librariesdirect.model.Library

@Deprecated("")
internal class LibraryAdapter(private val context: Activity, private val libraries: Array<Any>) :
    ArrayAdapter<Any?>(
        context, R.layout.row_library, libraries
    ) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val libraryView: View
        val lib = libraries[position] as Library
        libraryView = convertView ?: inflater.inflate(R.layout.row_library, parent, false)

        val textView = libraryView.findViewById<TextView>(R.id.text)
        val browseView = libraryView.findViewById<ImageView>(R.id.browse)
        textView.text = getText(lib, context)

        if (lib.url == null) {
            browseView.visibility = View.GONE
        } else {
            browseView.visibility = View.VISIBLE
            browseView.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(lib.url))
                context.startActivity(intent)
            }
        }

        textView.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle(R.string.license)
                .setMessage(lib.getDisplayMessage(context))
                .show()
        }

        return libraryView
    }
}