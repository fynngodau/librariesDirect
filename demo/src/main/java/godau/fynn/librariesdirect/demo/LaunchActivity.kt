/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to librariesDirect. This work is published from Germany.
 */
package godau.fynn.librariesdirect.demo

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import godau.fynn.librariesdirect.AboutDirectActivity
import godau.fynn.librariesdirect.model.Artwork
import godau.fynn.librariesdirect.model.ContextConsumer
import godau.fynn.librariesdirect.model.Contribution
import godau.fynn.librariesdirect.model.Fork
import godau.fynn.librariesdirect.model.Imprint
import godau.fynn.librariesdirect.model.Library
import godau.fynn.librariesdirect.model.License
import godau.fynn.librariesdirect.model.OwnLicense
import godau.fynn.librariesdirect.model.Translator
import java.util.Locale

@SuppressLint("CustomSplashScreen")
class LaunchActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        val about = findViewById<Button>(R.id.about)
        val open = findViewById<Button>(R.id.open)
        open.setOnClickListener { v: View? ->
            val intent = AboutDirectActivity.IntentBuilder(
                this@LaunchActivity,
                R.string.app_name,
                BuildConfig.VERSION_NAME
            )
                .setContent(
                    arrayOf(
                        Artwork(
                            what = "empty space",
                            license = License("public domain", null),
                            author = "nobody"
                        ),
                        Contribution(
                            license = License.CC0_LICENSE,
                            foreword = null,
                            author = "nobody",
                        ),
                        Library(
                            name = "Sample library 1",
                            license = License.CC0_LICENSE,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Library(
                            name = "Sample library 2",
                            license = License.APACHE_20_LICENSE,
                            foreword = null,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Library(
                            name = "Sample library 3",
                            license = License.GNU_GPL_V3_LICENSE,
                            author = "Fynn Godau"
                        ),
                        Library(
                            name = "Sample library 4",
                            license = License.GNU_GPL_V3_OR_LATER_LICENSE,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Library(
                            name = "Sample library 5",
                            license = License.MIT_LICENSE,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Library(
                            name = "Sample library 6",
                            license = License.CC0_LICENSE,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Library(
                            name = "Sample library 7",
                            license = License.APACHE_20_LICENSE,
                            author = "Fynn Godau"
                        ),
                        Library(
                            name = "Sample library 8",
                            license = License.GNU_GPL_V3_LICENSE,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Fork(
                            name = "templateDirect",
                            license = License.CC0_LICENSE,
                            author = "Fynn Godau"
                        )
                    )
                ) //.setHeaderText("This sample page demonstrates the functionality of librariesDirect.")
                .build()
            startActivity(intent)
        }
        about.setOnClickListener { v: View? ->
            val intent = AboutDirectActivity.IntentBuilder(
                this@LaunchActivity,
                R.string.app_name,
                BuildConfig.VERSION_NAME
            )
                .setAppDeveloperName("Fynn Godau")
                .setAppDeveloperMastodon(getString(R.string.developer_mastodon))
                .setIcon(R.mipmap.ic_launcher)
                .setContent(
                    arrayOf(
                        Translator("Fynn Godau", "fynngodau", Locale.GERMANY),
                        Library(
                            name = getString(R.string.app_name) + ' ' + BuildConfig.VERSION_NAME,
                            license = License.CC0_LICENSE,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Library(
                            name = "Annotation",
                            license = License.APACHE_20_LICENSE,
                            author = "The Android Open Source Project",
                            url = "https://developer.android.com/jetpack/androidx/releases/annotation"
                        ),
                        Library(
                            name = "AppCompat",
                            license = License.APACHE_20_LICENSE,
                            author = "The Android Open Source Project",
                            url = "https://developer.android.com/jetpack/androidx/releases/annotation"
                        ),
                        Library(
                            name = "librariesDirect",
                            license = License.CC0_LICENSE,
                            author = "Fynn Godau",
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        OwnLicense(
                            license = License.CC0_LICENSE,
                            url = "https://codeberg.org/fynngodau/librariesDirect"
                        ),
                        Imprint(
                            "Fynn Godau\n" +
                                    "Stößelstraße 6\n" +
                                    "97422 Schweinfurt\n" +
                                    "Deutschland\n" +
                                    "\n" +
                                    "fynngodau@mailbox.org\n" +
                                    "+49 9721 730335\n" +
                                    "\n" +
                                    "Umsatzsteuer-Identifikationsnummer (VAT identification number): DE347187327\n" +
                                    "\n" +
                                    "Plattform der EU zur außergerichtlichen Streitbeilegung\n" +
                                    "(EU platform for Online Dispute Resolution):\n" +
                                    "https://ec.europa.eu/consumers/odr/"
                        )
                    )
                )
                .setConsumer(object : ContextConsumer {
                    override fun accept(t: Context) {
                        t.setTheme(android.R.style.Theme_Holo)
                    }
                })
                .build()
            startActivity(intent)
        }
    }
}