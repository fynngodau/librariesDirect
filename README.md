# aboutDirect

A library to display an about screen in an Android app. It was created for use with other Direct projects but may be
used independently as well.

```
minSdkVersion 14
targetSdkVersion 28
```

## Including in your project

Please use Jitpack to download compiled AAR packages. See how [here](https://jitpack.io/#org.codeberg.fynngodau/librariesDirect/).

## Usage

### Getting an `Intent`

To call `AboutDirectActivity`, you have to use the builder class `AboutDirectActivity.IntentBuilder` like seen here:

    new AboutDirectActivity.IntentBuilder(context, R.string.app_name, BuildConfig.VERSION_NAME)
        .setContent(…)
        .build();

Since version 3.1, the about screen is also available as a Fragment. Simply call `buildFragment()` at the end instead.

These are the calls that you can use to configure the builder class:

#### `setContent(Object[])`

Pass an array of `Object`s.

Valid `Object`s are:

* `Artwork`
* `Fork`
* `Library`
* `License`
* `OwnLicense`
* `Translator`
* `Contribution`
* `Imprint`

Each `Object` will be displayed by the About screen. All of them have similar constructors and look slightly different
to each other when viewed.

For example, here's how a library is constructed:

    new Library(String name, License license, @Nullable String foreword, String author, String url)

* `name`: Name of the library
* `license`: A license object. The `License` class has a small selection of licenses already hardcoded for you, including MIT and Apache 2.0, but you can construct your own license using `new License(String name, String licenseText)` as well.
* `foreword`: Some people put one line or two of copyright statement above their paste of e.g. the MIT license. Use the foreword string in that case, or pass null.
* `author`: Should fit into this sentence: "`author` is distributing this library."
* `plural` (optional): Whether author is a word that needs to be referred to in plural
* `url`: The website that the library can be found at. May be null, in that case the "open externally" button will be hidden.

For example, if you wanted to credit aboutDirect, add this line to your `Library` array:

    new Library("aboutDirect", License.CC0_LICENSE, null, "Fynn Godau", "https://codeberg.org/fynngodau/librariesDirect"),

For the other classes, JavaDoc explains how to use them.

#### `setIcon(@DrawableRes int)`

Pass an icon resource to display it at the top to the left of the app's name.

#### `setAppDeveloperName(String)`

Pass the app developer's name to display it at the top below you app's name.

#### `setAppDeveloperMastodon(String)`

Pass a URL to display a Mastodon icon to the right of the developer's name that links to that URL.

#### `setConsumer(ContextConsumer)`

Pass an implementation of the `ContextConsumer` interface in case there is something you need to do after the activity
has launched, like setting style dynamically.

When used with `buildFragment()`, the consumer is executed by the fragment on creation, but before the layout is created. 

Therefore, the activities `Context` is provided to you in the interface's `accept(Context)` method immediately before
the layout is inflated.

Note that the `ContextConsumer` interface extends the `Serializable` interface. As such, all fields that your
implementation has must also be `Serializable`. If you create a `ContextConsumer` implementation as a non-static inner
class of an `Activity` (possibly by creating an anonymous class), the app will crash.

#### `build()`

Once you are done, call `build()` on the builder class to receive an `Intent` which you can start using `startActivity(…)`.

## Version 3 migration

* The namespace of some classes changed. You need to fix your imports.
* Consider switching to `AboutDirectActivity` instead of `AboutLibrariesActivity`.
`AboutLibrariesActivity` has been deprecated and may be removed in a future release.

## License

Because I think it would have been annoying to have to credit a library that is used to credit libraries, this library
is licensed under CC0; no credits are required, even though they are welcome.

## Screenshot

![Screenshot](https://codeberg.org/fynngodau/librariesDirect/raw/branch/master/screenshot.png)
